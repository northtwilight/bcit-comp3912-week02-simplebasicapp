//
//  ViewController.swift
//  COMP3912-Week02-SimpleBasicApp
//
//  Created by Massimo Savino on 2016-09-21.
//  Copyright © 2016 Massimo Savino. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    // MARK: Properties
    
    @IBOutlet weak var imageLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageButton: UIButton!
    
    var increment: Int = 0
    var imageNames: [String]? = []
    var isTheFirstTime: Bool = true
    
    
    
    // MARK: Initialization
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
    }
    
    
    
    // MARK: Actions
    
    @IBAction func changeImage(sender: UIButton) {
        
        incrementActiveImage()
    }
    
    
    
    // MARK: Lifecycle

    override func viewDidLoad() {
        
        super.viewDidLoad()
        setUp()
    }
    
    
    
    // MARK: Set up UI
    
    func setUp() {
        
        createImageArray()
        loadRandomImage()
        
        imageButton.setTitle("Change photo image", for: .normal)
        imageButton.titleLabel!.font = UIFont(name: "Futura", size: 16)
        self.isTheFirstTime = true
    }
 
    
    
    // MARK: Helper functions
    
    func loadRandomImage() {
        
        let randomIncrement = arc4random_uniform(6)
        
        increment = Int(randomIncrement)
        print("initialised starting Int is \(randomIncrement)")
        
        let name = "image\(increment).png"
        imageView.image = UIImage(named: name)
        
        imageLabel.text = "Title: " + name
        imageLabel.font = UIFont(name: "Futura", size: 16)
        
        isTheFirstTime = true
    }
    
    
    
    func createImageArray() -> [String] {
        
        for item in 0...6 {
            
            let name = "image\(item).png"
            imageNames?.append(name)
            
            increment += 1
        }
        
        guard let images = imageNames else {
            return []
        }
        
        print("array of image names is \(images),\ntotal count is \(images.count)")
        
        return images
    }
    
    
    
    func incrementActiveImage() {

        if let images = imageNames, let count = imageNames?.count {
            
            let name: String?
            
            if isTheFirstTime == true {
                
                print("Starting increment: \(increment)")
                increment += 1
                
            }
            
            print("The increment: \(increment)")
            
            if increment < count {
                
                if count < increment + 1 {
                    name = images[0]
                    if isTheFirstTime == true {
                        increment = 1
                    }
                    
                } else {
                    name = images[increment]
                    increment += 1
                }
            }
            
            else {
                name = images[0]
                increment = 1
            }
            
            imageView.image = UIImage(named: name!)
            imageLabel.text = "Title: " + name!
            
            print("Current image name is \(name!)")
            
            isTheFirstTime = false
        }
    }
}

